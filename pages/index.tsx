export default () => (
  <div>
    <div style={{ padding: "25% 25% 10% 25%" }}>
      <h2>
        left blank llc
      </h2>
      <h3> is a software consultancy specializing in react native,
        flutter, and other hybrid mobile apps.</h3>
      <p>
        contact brandon via <a href="hello@leftblank.co">hello@leftblank.co</a> for more
        information
      </p>
    </div>

    <p style={{ padding: "0 0 25% 25%" }}>(This page intentionally left blank.)</p>
  </div>
);
